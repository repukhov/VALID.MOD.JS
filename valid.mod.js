/*
	Create:
	- <input name=#name_phone# value=""/>
	- <input name=#name_email# value=""/>
    check.phone('#name_phone#', 'Телефон введён неверно!');
	check.email('#name_email#', 'E-Mail введён неверно!');
*/

;(function(){

	var object = {

		phone: function(name, msg){

			var field = document.getElementsByTagName('input');
			for (var i = 0; i < field.length; i++)
			{
			     if(field[i].name == name)
			     {
					//var re = /^\+?\d[\d\(\)\ -]{3,17}\d$/;
		 			var re = /^\+7[0-9]{10,17}$/;
		 			var valid = re.test(field[i].value);
		 			if(valid)
		 			{
		 				if(field[i].classList.contains("vldphone"))
		 				{
		 					field[i].classList.remove("vldphone");
		 					field[i].style.boxShadow = '';
		 					field[i].parentElement.removeChild(field[i].nextSibling);
		 				}

		 				return true;
		 			}
		 			else
		 			{
		 				var span = document.createElement('span');
		 				span.style.position 	= 'absolute';
		 				span.style.zIndex 		= '221';
		 				span.className 			= 'vlderror';
		 				span.style.marginLeft 	= '10px';
		 				span.style.color 		= 'red';
		 				span.style.background 	= 'yellow';
		 				span.style.padding 		= '0 3px 0 3px';
		 				span.style.border 		= '1px solid red';
		 				span.innerHTML 			= '&lt; ' + msg;

	 					if(!field[i].nextSibling){

		 					field[i].parentElement.appendChild(span);
		 				}
		 				else
		 				{
		 					if(field[i].nextSibling.className != 'vlderror')
		 					{
		 						field[i].parentElement.insertBefore(span,field[i].nextSibling);
		 					}	
		 				}	
			 						 				
		 				if(!field[i].classList.contains("vldphone"))
		 				{
		 					field[i].classList.add("vldphone");
		 					field[i].style.boxShadow = '0 0 5px 1px red';
		 				}	
		 				
		 				return false;
		 			}
			     }	
		    }
		},

		email: function(name, msg){
			var field = document.getElementsByTagName('input');
			for (var i = 0; i < field.length; i++)
			{
			     if(field[i].name == name)
			     {
					var re = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
		 			var valid = re.test(field[i].value);
		 			if(valid)
		 			{
		 				if(field[i].classList.contains("vldemail"))
		 				{
		 					field[i].classList.remove("vldemail");
		 					field[i].parentElement.removeChild(field[i].nextSibling);
		 				}

		 				field[i].style.boxShadow = '';
		 				return true;
		 			}
		 			else
		 			{

		 				var span = document.createElement('span');
		 				span.style.position 	= 'absolute';
		 				span.style.zIndex 		= '221';
		 				span.className 			= 'vlderror';
		 				span.style.marginLeft 	= '10px';
		 				span.style.color 		= 'red';
		 				span.style.background 	= 'yellow';
		 				span.style.border 		= '1px solid red';		 				
		 				span.style.padding 		= '0 3px 0 3px';
		 				span.innerHTML 			= '&lt; ' + msg;

		 				if(!field[i].nextSibling){

		 					field[i].parentElement.appendChild(span);
		 				}
		 				else
		 				{
		 					if(field[i].nextSibling.className != 'vlderror')
		 					{
		 						field[i].parentElement.insertBefore(span,field[i].nextSibling);
		 					}
		 				}

		 				if(!field[i].classList.contains("vldemail"))
		 				{
		 					field[i].classList.add("vldemail");
		 					field[i].style.boxShadow = '0 0 5px 1px red';		 					
		 				}			 				

		 				
		 				return false;
		 			}
			     }		     
		    }			

		},

		inn: function(name, msg){
			var field = document.getElementsByTagName('input');
			for (var i = 0; i < field.length; i++)
			{
			     if(field[i].name == name)
			     {
					var re = /^[0-9]{10,12}$/;
		 			var valid = re.test(field[i].value);
		 			if(valid)
		 			{
		 				if(field[i].classList.contains("vldemail"))
		 				{
		 					field[i].classList.remove("vldemail");
		 					field[i].parentElement.removeChild(field[i].nextSibling);
		 				}

		 				field[i].style.boxShadow = '';
		 				return true;
		 			}
		 			else
		 			{

		 				var span = document.createElement('span');
		 				span.style.position 	= 'absolute';
		 				span.style.zIndex 		= '221';
		 				span.className 			= 'vlderror';
		 				span.style.marginLeft 	= '10px';
		 				span.style.color 		= 'red';
		 				span.style.background 	= 'yellow';
		 				span.style.border 		= '1px solid red';		 				
		 				span.style.padding 		= '0 3px 0 3px';
		 				span.innerHTML 			= '&lt; ' + msg;

		 				if(!field[i].nextSibling){

		 					field[i].parentElement.appendChild(span);
		 				}
		 				else
		 				{
		 					if(field[i].nextSibling.className != 'vlderror')
		 					{
		 						field[i].parentElement.insertBefore(span,field[i].nextSibling);
		 					}
		 				}

		 				if(!field[i].classList.contains("vldemail"))
		 				{
		 					field[i].classList.add("vldemail");
		 					field[i].style.boxShadow = '0 0 5px 1px red';		 					
		 				}			 				

		 				
		 				return false;
		 			}
			     }		     
		    }			

		},
		password: function(name, msg){
			var field = document.getElementsByTagName('input');
			for (var i = 0; i < field.length; i++)
			{
			     if(field[i].name == name)
			     {
					var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&_\-\(\)\+\=])[A-Za-z\d$@$!%*?&_\-\(\)\+\=]{8,12}$/;
		 			var valid = re.test(field[i].value);
		 			if(valid)
		 			{
		 				if(field[i].classList.contains("vldemail"))
		 				{
		 					field[i].classList.remove("vldemail");
		 					field[i].parentElement.removeChild(field[i].nextSibling);
		 				}

		 				field[i].style.boxShadow = '';
		 				return true;
		 			}
		 			else
		 			{

		 				var span = document.createElement('span');
		 				span.style.position 	= 'absolute';
		 				span.style.zIndex 		= '221';
		 				span.className 			= 'vlderror';
		 				span.style.marginLeft 	= '10px';
		 				span.style.color 		= 'red';
		 				span.style.background 	= 'yellow';
		 				span.style.border 		= '1px solid red';		 				
		 				span.style.padding 		= '0 3px 0 3px';
		 				span.innerHTML 			= '&lt; ' + msg;

		 				if(!field[i].nextSibling){

		 					field[i].parentElement.appendChild(span);
		 				}
		 				else
		 				{
		 					if(field[i].nextSibling.className != 'vlderror')
		 					{
		 						field[i].parentElement.insertBefore(span,field[i].nextSibling);
		 					}
		 				}

		 				if(!field[i].classList.contains("vldemail"))
		 				{
		 					field[i].classList.add("vldemail");
		 					field[i].style.boxShadow = '0 0 5px 1px red';		 					
		 				}			 				

		 				
		 				return false;
		 			}
			     }		     
		    }			

		},
		login: function(name, msg){
			var field = document.getElementsByTagName('input');
			for (var i = 0; i < field.length; i++)
			{
			     if(field[i].name == name)
			     {
					var re = /^(?=.*[a-z])(?=.*[A-Z])[A-Za-z\d]{6,10}$/;
		 			var valid = re.test(field[i].value);
		 			if(valid)
		 			{
		 				if(field[i].classList.contains("vldemail"))
		 				{
		 					field[i].classList.remove("vldemail");
		 					field[i].parentElement.removeChild(field[i].nextSibling);
		 				}

		 				field[i].style.boxShadow = '';
		 				return true;
		 			}
		 			else
		 			{

		 				var span = document.createElement('span');
		 				span.style.position 	= 'absolute';
		 				span.style.zIndex 		= '221';
		 				span.className 			= 'vlderror';
		 				span.style.marginLeft 	= '10px';
		 				span.style.color 		= 'red';
		 				span.style.background 	= 'yellow';
		 				span.style.border 		= '1px solid red';		 				
		 				span.style.padding 		= '0 3px 0 3px';
		 				span.innerHTML 			= '&lt; ' + msg;

		 				if(!field[i].nextSibling){

		 					field[i].parentElement.appendChild(span);
		 				}
		 				else
		 				{
		 					if(field[i].nextSibling.className != 'vlderror')
		 					{
		 						field[i].parentElement.insertBefore(span,field[i].nextSibling);
		 					}
		 				}

		 				if(!field[i].classList.contains("vldemail"))
		 				{
		 					field[i].classList.add("vldemail");
		 					field[i].style.boxShadow = '0 0 5px 1px red';		 					
		 				}			 				

		 				
		 				return false;
		 			}
			     }		     
		    }			

		},

		typeorg: function(name1, name2, msg){
			var field = document.querySelectorAll('input[type="checkbox"]');
			console.log(field);
			

		},

		fio: function(){
			
		}

	}
	window.Validate = object;
})();